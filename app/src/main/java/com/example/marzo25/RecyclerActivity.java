package com.example.marzo25;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class RecyclerActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private ArrayList<String> datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        recyclerView = findViewById(R.id.recyclerView);

        // si quieren - fixed size - si sabemos que no cambia esto lo agiliza un poquito
        recyclerView.setHasFixedSize(true);

        // declarar datos - OBVIAMENTE los pueden obtener de otro lado
        datos = new ArrayList<>();
        datos.add("Gus");
        datos.add("Alexis");
        datos.add("Hector");
        datos.add("Mayra");
        datos.add("Joachin");
        datos.add("Pietra");

        // inicializamos adaptador
        StudentAdapter adapter = new StudentAdapter(datos, this);

        // layout manager
        // define como se va a organizar los elementos del recycler view
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        GridLayoutManager glm = new GridLayoutManager(this, 2);


        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {

        int pos = recyclerView.getChildLayoutPosition(v);
        Toast.makeText(this, datos.get(pos), Toast.LENGTH_SHORT).show();
    }
}
